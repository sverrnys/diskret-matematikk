# SKAL LAGE SANNHETSTABELL FOR N ANTALL VARIABLER

# variabler: 2 er 4
# variabler: 3 er 8
# variabler: 4 er 8

# Variabler 2^n

# Mønstret for T og F: TRUE FALSE. er første har halvparten er true resten false, så er det intervaller hvor (summen av variabler - indexen til variablen)
# A
# B 3 T og 3 F til summen blir 2^n
# C 2 T og 2 F til summen blir 2^n
# D 1 T og 1 F til summen blir 2^n

# antall variabler


def assign_true_false():
    # Fjerner alt whitespace og deler stringen inn i en array som skiller entries med ,

    variables = str(input("Write the variables: (Seperate by ',') "))
    variable_list = (variables.replace(" ", "")).split(",")
    n = len(variable_list)  # int(input("How many variables are used: "))
    length_of_table = 2**n
    truth_table = []  # [[], [], [], []]
    # ønsker antall variabler med arrayer
    for i in range(n):
        truth_table.append([])
        if i == 0:
            for j in range(length_of_table):
                if j < length_of_table/2:
                    truth_table[i].append(0)
                else:
                    truth_table[i].append(1)
        else:
            truth_table[i] = intervals(n-i, length_of_table)

    print(variable_list, length_of_table, truth_table)

def intervals(interval_length, array_length):
    # Skal sette inn 1 og 0 i riktige intervaller
    array = []
    for x in range(int(array_length/interval_length)):
        # 16 / 2^3 == 2
        if x % 2 == 1:
            array += list((2**(interval_length-1))*"1")
        else:
            array += list((2**(interval_length-1))*"0")
    for i in range(len(array)):
        array[i] = int(array[i])
    return array


print(bool(0),bool(1))
# def disjunction(array):  # A eller B
    # for i in range(len(array))


# def de_morgens_laws(array):


assign_true_false()
